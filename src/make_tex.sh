#!/usr/bin/env bash

echo "Producing pdf file... please wait..."

echo "\documentclass[11pt, a3paper ]{article}
\usepackage[margin=1cm, top=2cm]{geometry}
\usepackage[bguq]{frege}
\usepackage{amsmath}
\usepackage{fancyhdr}

\pagestyle{fancy}
\fancyhf{}
\lhead{output produced at $(date)}
\rhead{FregeIT}


\begin{document}


\bf{[proposition]} \$ $1 \$

\\ \quad \\
\newline

\bf{[translation]} \$ $2 \$

\\ \quad \\
\\ \quad \\
\\ \quad \\
\newline

$(cat f_out)



\end{document}
" > tex/out.tex


sed -i 's/->/ \\rightarrow /g' tex/out.tex
sed -i 's/\\\// \\lor /g' tex/out.tex
sed -i 's/\/\\/ \\land /g' tex/out.tex
sed -i 's/~/ \\neg /g' tex/out.tex

pdflatex -quiet -output-directory ../output tex/out.tex > /dev/null 2>&1

evince ../output/out.pdf &
