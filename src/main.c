/*
 * main.c file
 */

#include "Expression.h"
#include "Parser.h"
#include "Lexer.h"
#include "string.h"

#include <stdio.h>

int yyparse(SExpression **expression, yyscan_t scanner);

SExpression *getAST(const char *expr)
{
    SExpression *expression;
    yyscan_t scanner;
    YY_BUFFER_STATE state;

    if (yylex_init(&scanner)) {
        /* could not initialize */
        return NULL;
    }

    state = yy_scan_string(expr, scanner);

    if (yyparse(&expression, scanner)) {
        /* error parsing */
        return NULL;
    }

    yy_delete_buffer(state, scanner);

    yylex_destroy(scanner);

    return expression;
}


char * toStr(char c) {
  static char str[10];
  sprintf(str, " %c ", c);
  //printf ("%s \n", str);
  return str;
}

char empty[] = "";

char * convert(SExpression *e, char * buffer)
{

    char str_left[10000];
    char str_right[10000];

    switch (e->type) {
        case eVALUE:
            return toStr(e->value);
        case eMULTIPLY:
            strcpy(str_left, convert(e->left, buffer));
            strcpy(str_right, convert(e->right, buffer));
            sprintf (buffer, "~(%s -> ~ %s)", str_left, str_right);
            return buffer;
        case eADD:
            strcpy(str_left, convert(e->left, buffer));
            strcpy(str_right, convert(e->right, buffer));
            sprintf (buffer, "(~ %s -> %s) ", str_right, str_left);
            return buffer;
        case eIMPLY:
            strcpy(str_left, convert(e->left, buffer));
            strcpy(str_right, convert(e->right, buffer));
            sprintf (buffer, "(%s -> %s)", str_left, str_right);
            return buffer;
        case eNOT:
            strcpy(str_right, convert(e->right, buffer));
            sprintf (buffer, "~ %s", str_right);
            return buffer;
        default:
            return empty;
    }
}

char * fregeit(SExpression *e, char * buffer)
{

    char str_left[10000];
    char str_right[10000];

    switch (e->type) {
        case eVALUE:
            return toStr(e->value);
        case eMULTIPLY:
            strcpy(str_left, fregeit(e->left, buffer));
            strcpy(str_right, fregeit(e->right, buffer));
            sprintf (buffer, "\\Fconditional[\\Fncontent] {\\Fncontent %s} {\\Fcontent %s}", str_right, str_left);
            return buffer;
        case eADD:
            strcpy(str_left, fregeit(e->left, buffer));
            strcpy(str_right, fregeit(e->right, buffer));
            sprintf (buffer, "\\Fconditional {\\Fcontent %s} {\\Fncontent %s}", str_left, str_right);
            return buffer;
        case eIMPLY:
            strcpy(str_left, fregeit(e->left, buffer));
            strcpy(str_right, fregeit(e->right, buffer));
            sprintf (buffer, "\\Fconditional {\\Fcontent %s} {\\Fcontent %s}", str_right, str_left);
            return buffer;
        case eNOT:
            strcpy(str_right, fregeit(e->right, buffer));
            sprintf (buffer, "\\Fncontent %s", str_right);
            return buffer;
        default:
            return empty;
    }
}

void writeOutput(char * out) {
  FILE *fp = fopen ("f_out", "w");

  fwrite (out, sizeof(char), strlen(out), fp);
  fclose(fp);

  return;
}

int main(int argc, char *argv[])
{
    if (argc <= 1)
      printf ("\nMissing proposition.\n\n");
    else {
      system ("clear");
      // "( ((P \\/ Q) \\/ (Z /\\ R) /\\ (P \\/ Q) \\/ (Z /\\ R)) /\\ ((P \\/ Q) \\/ (Z /\\ R) /\\ (P \\/ Q) \\/ (Z /\\ R)) )"
      char proposition[100000];
      strcpy(proposition, argv[1]);
      SExpression *e = getAST(proposition);
      char result[100000], output[100000];
      char conv[100000];
      fregeit(e, result);
      convert(e, conv);

      printf("\n******************************\n| Welcome to FregeIT v. 0.1! |");
      printf("\n******************************\n \nHere is your output:\n\n");
      printf("Original: %s \n\n", proposition);
      printf("Converted: %s \n\n", conv);
      printf("Equivalence relation: %s <-> %s\n\n", proposition, conv);

      printf("Fregefied: \n\n");
      printf("\\Facontent  %s \n\n", result);

      sprintf (output, "\\Facontent  %s", result);
      writeOutput(output);

      char command[100000];

      sprintf (command, "./make_tex.sh \"%s\" \"%s\"", proposition, conv);
      system (command);

      deleteExpression(e);
    }
    return 0;
}
